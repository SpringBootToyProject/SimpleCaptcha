package com.springtoyproject.simplecaptcha.control.security;

import nl.captcha.Captcha;
import nl.captcha.backgrounds.FlatColorBackgroundProducer;
import nl.captcha.noise.CurvedLineNoiseProducer;
import nl.captcha.noise.StraightLineNoiseProducer;
import nl.captcha.text.producer.NumbersAnswerProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class CaptchaGenerator {

    @Autowired
    private ApplicationContext context;

    @Bean("RandomLenGenerator")
    public Random initializeRandom() {
        return new Random();
    }

    public Captcha createCaptcha(int width, int height) {
        Random rnd = context.getBean("RandomLenGenerator", Random.class);
        int len = rnd.nextInt(2);
        Captcha.Builder builder = new Captcha.Builder(width, height)
                .addBackground(new FlatColorBackgroundProducer())
                .addText(new NumbersAnswerProducer(len + 5))
                .addNoise(new CurvedLineNoiseProducer())
                .addNoise(new StraightLineNoiseProducer());
        return builder.build();
    }

}
