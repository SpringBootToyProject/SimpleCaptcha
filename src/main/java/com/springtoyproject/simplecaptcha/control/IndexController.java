package com.springtoyproject.simplecaptcha.control;

import com.springtoyproject.simplecaptcha.control.security.CaptchaGenerator;
import com.springtoyproject.simplecaptcha.control.security.CaptchaUtils;
import nl.captcha.Captcha;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller

public class IndexController {

    @Autowired
    private CaptchaGenerator captchaGenerator;

    @Value("${captcha.session.variableName}")
    private String captchaSessionVariableName;

    @GetMapping(value = "/")
    public String login(Model model, HttpSession session) {
        Captcha captcha = captchaGenerator.createCaptcha(200, 50);
        session.setAttribute(this.captchaSessionVariableName, captcha);
        String attributeValue = CaptchaUtils.encodeBase64(captcha);
        model.addAttribute("captchaEnc", attributeValue);
        // }
        return "index";
    }

    @PostMapping("/checkCaptcha")
    public String getCaptcha(@RequestParam(name = "captcha", required = false) String captcha,
                             HttpServletRequest req,
                             RedirectAttributes redirectAttributes) {
        Captcha sessionCaptcha = (Captcha) req.getSession().getAttribute(captchaSessionVariableName);
        String answer = sessionCaptcha.getAnswer();
        Boolean res = captcha.equals(answer);
        redirectAttributes.addFlashAttribute("isValidCaptcha", res);
        return "redirect:/captchaResult";
    }

    @GetMapping("/captchaResult")
    public String captchaResultPage(Model model) {
        return "captchaResult";
    }
}
